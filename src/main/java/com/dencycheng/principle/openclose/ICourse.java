package com.dencycheng.principle.openclose;

/**
 * @Author dencycheng
 * @Date 2020/2/16 16:07
 */
public interface ICourse {

    Integer getId();
    String getName();
    Double getPrice();
}
