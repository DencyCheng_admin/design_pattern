package com.dencycheng.observer;

/**
 * @author dencycheng
 * @date 2020/1/2 19:50
 */
public abstract class Observer {
    protected Subject subject;

    public abstract void update();
}
