package com.dencycheng.chain;

/**
 * @author dencycheng
 * @date 2020/1/14 9:54
 */
public class FileLogger extends AbstractLogger {

    public FileLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("File::Logger: " + message);
    }
}

