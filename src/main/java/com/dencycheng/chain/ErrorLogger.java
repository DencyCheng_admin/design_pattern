package com.dencycheng.chain;

/**
 * @author dencycheng
 * @date 2020/1/14 9:53
 */
public class ErrorLogger extends AbstractLogger {

    public ErrorLogger(int level){
        this.level = level;
    }

    @Override
    protected void write(String message) {
        System.out.println("Error Console::Logger: " + message);
    }
}