package com.dencycheng.strategy.type2;

/**
 * @author dencycheng
 * @date 2019/12/2 14:19
 */
@TypeHandle(Type.TYPE1)
public class OperationType1 implements Operation {
    @Override
    public void handle(Integer count) {
        System.out.println("我是1打印的：" + count);
    }
}
