package com.dencycheng.strategy.type2;

/**
 * @author dencycheng
 * @date 2019/12/2 14:19
 */
@TypeHandle(Type.TYPE2)
public class OperationType2 implements Operation {
    @Override
    public void handle(Integer count) {
        System.out.println("我是2打印的：" + count);
    }
}
