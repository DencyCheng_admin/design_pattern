package com.dencycheng.strategy.type2;

import java.util.Objects;

/**
 * @author dencycheng
 * @date 2019/12/2 14:17
 */
public enum Type {

    /**
     * 类型1
     */
    TYPE1(1,"处理1"),
    /**
     * 类型2
     */
    TYPE2(2,"处理2"),
    /**
     * 类型3
     */
    TYPE3(3,"处理3");


    private Integer type;

    private String name;

    Type(Integer type, String name) {
        this.type = type;
        this.name = name;
    }

    public Integer getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Type from(Integer value) {
        Type[] values = Type.values();
        for (Type v : values) {
            if (Objects.equals(v.type, value)) {
                return v;
            }
        }
        return null;
    }
}
