package com.dencycheng.strategy.type2;

import cn.hutool.core.lang.ClassScaner;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author dencycheng
 * @date 2019/12/2 14:19
 */
public class HandleDispatcher {

    private static Map<Type,Operation> dispatcherMap = new HashMap<>();

    public void handle(Integer type,Integer count){
            Type typeEnum = Type.from(type);
            Operation operation = dispatcherMap.get(typeEnum);
            operation.handle(count);
    }

    /**
     * 初始化
     */
    public HandleDispatcher() {
        Set<Class<?>> classes = ClassScaner.scanPackage("com.dencycheng.strategy.type2");
        classes.forEach(x->{
            TypeHandle annotation = x.getAnnotation(TypeHandle.class);
            if(annotation != null){
                Object o = null;
                try {
                     o = x.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                dispatcherMap.put(annotation.value(),(Operation) o);
            }
        });
    }
}
