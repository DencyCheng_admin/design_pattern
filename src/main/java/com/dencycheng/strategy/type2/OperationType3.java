package com.dencycheng.strategy.type2;

/**
 * @author dencycheng
 * @date 2019/12/2 14:19
 */
@TypeHandle(Type.TYPE3)
public class OperationType3 implements Operation {
    @Override
    public void handle(Integer count) {
        System.out.println("我是3打印的：" + count);
    }
}
