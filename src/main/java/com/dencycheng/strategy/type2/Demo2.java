package com.dencycheng.strategy.type2;

/**
 * @author dencycheng
 * @date 2019/12/2 17:56
 */
public class Demo2 {
    public static void main(String[] args) {
        HandleDispatcher handleDispatcher = new HandleDispatcher();
        handleDispatcher.handle(2,5);
    }
}
