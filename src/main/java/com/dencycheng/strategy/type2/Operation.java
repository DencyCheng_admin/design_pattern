package com.dencycheng.strategy.type2;

/**
 * @author dencycheng
 * @date 2019/12/2 14:04
 */
public interface Operation {
    /**
     * 操作
     * @param count
     */
    void handle(Integer count);
}
