package com.dencycheng.strategy.type1;

/**
 * 计算
 * @author dencycheng
 * @date 2019/12/2 14:09
 */
public class Calculate {

    private Operation operation;

    public Calculate(Operation operation) {
        this.operation = operation;
    }

    public void handle(Integer a,Integer b){
        System.out.println(operation.handle(a,b));
    }
}
