package com.dencycheng.strategy.type1;

/**
 * 加法
 * @author dencycheng
 * @date 2019/12/2 14:05
 */
public class OperationAdd implements Operation {
    @Override
    public Integer handle(Integer a, Integer b) {
        return a + b;
    }
}
