package com.dencycheng.strategy.type1;

/**
 * 我们将创建一个定义活动的 Operation 接口和实现了 Operation 接口的实体策略类。Calculate 是一个使用了某种策略的类。
 *
 * Demo1，我们的演示类使用 Calculate 和策略对象来演示 Context 在它所配置或使用的策略改变时的行为变化。
 *
 * @author dencycheng
 * @date 2019/12/2 14:04
 */
public interface Operation {
    /**
     * 计算
     * @param a
     * @param b
     * @return
     */
    Integer handle(Integer a,Integer b);
}
