package com.dencycheng.strategy.type1;

import java.util.Calendar;

/**
 * @author dencycheng
 * @date 2019/12/2 14:11
 */
public class Demo1 {

    public static void main(String[] args) {
        //初始化Add操作
        Calculate calculate = new Calculate(new OperationAdd());
        calculate.handle(2,1);
        //初始化Substract操作
        Calculate calculate1 = new Calculate(new OperationSubstract());
        calculate1.handle(2,1);
    }
}
