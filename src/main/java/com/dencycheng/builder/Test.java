package com.dencycheng.builder;

/**
 * @Author dencycheng
 * @Date 2020/2/18 20:48
 */
public class Test {
    public static void main(String[] args) {
        CourseBuilder courseBuilder = new CourseActualBuilder();
        Cocah cocah = new Cocah();
        cocah.setCourseBuilder(courseBuilder);

        Course course = cocah.createCourse("java设计模式","java设计模式-PPT"
                                    ,"java设计模式-Article","java设计模式-QA","java设计模式-Video");
    }
}
