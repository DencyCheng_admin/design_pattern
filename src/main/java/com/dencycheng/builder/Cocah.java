package com.dencycheng.builder;

/**
 * @Author dencycheng
 * @Date 2020/2/18 20:40
 */
public class Cocah {
    private CourseBuilder courseBuilder;

    public void setCourseBuilder(CourseBuilder courseBuilder) {
        this.courseBuilder = courseBuilder;
    }

    public Course createCourse(String courseName,String coursePPT,String courseArticle,String courseQA,String video){
        courseBuilder.buildCourseName(courseName);
        courseBuilder.buildCoursePPT(coursePPT);
        courseBuilder.buildCourseArticle(courseArticle);
        courseBuilder.buildCourseQA(courseQA);
        courseBuilder.buildCourseVideo(video);
        return this.courseBuilder.create();
    }
}
