package com.dencycheng.builder.v2;

/**
 * @Author dencycheng
 * @Date 2020/2/18 21:12
 */
public class Test {
    public static void main(String[] args) {
        Course course = new Course.CourseBuilder().buildCourseName("java 设计模式").build();
        System.out.println(course);
    }
}
