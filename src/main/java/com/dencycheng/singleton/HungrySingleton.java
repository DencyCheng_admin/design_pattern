package com.dencycheng.singleton;

import java.io.Serializable;

/**
 * 饿汉式
 * @Author dencycheng
 * @Date 2019/12/15 20:52
 */
public class HungrySingleton implements Serializable {
    private final static HungrySingleton hungrySingleton = new HungrySingleton();
    
    private HungrySingleton(){
        
    }

    public static HungrySingleton getInstance() {
        return hungrySingleton;
    }

    /**
     * 防止反序列化反射出新的方法
     * @return
     */
    public Object readResolve(){
        return hungrySingleton;
    }
}
