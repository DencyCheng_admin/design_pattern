package com.dencycheng.singleton;

/**
 * 静态内部类
 *
 * 调用类的静态成员(非字符串常量)的时候会导致类(InnerClass)的初始化.
 * 并且在执行类的初始化期间,JVM 会获取一个初始化锁,这个锁可以同步多个线程对同一个类的初始化.
 *
 * @Author dencycheng
 * @Date 2019/12/15 20:41
 */
public class StaticInnerClassSingleton {

    private StaticInnerClassSingleton() {

    }

    private static class InnerClass {
        private static StaticInnerClassSingleton staticInnerClassSingleton = new StaticInnerClassSingleton();
    }

    public static StaticInnerClassSingleton getInstance() {
        return InnerClass.staticInnerClassSingleton;
    }
}
