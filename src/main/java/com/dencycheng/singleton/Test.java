package com.dencycheng.singleton;


import com.dencycheng.singleton.T;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class Test {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
//        Thread t1 = new Thread(new T());
//        Thread t2 = new Thread(new T());
//        t1.start();
//        t2.start();
//        System.out.println("end");

//        HungrySingleton instance = HungrySingleton.getInstance();
        EnumSingleton instance = EnumSingleton.getInstance();
        instance.setData(new Object());
        //反序列化
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("hungry"));
        oos.writeObject(instance);

        File file = new File("hungry");
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
        EnumSingleton newInstance = (EnumSingleton) ois.readObject();
        System.out.println(instance.getData());
        System.out.println(newInstance.getData());
        System.out.println(instance.getData() == newInstance.getData());

        Class enumSingletonClass = EnumSingleton.class;
        Constructor declaredConstructor = enumSingletonClass.getDeclaredConstructor(String.class,int.class);
        declaredConstructor.setAccessible(true);
        EnumSingleton dencycheng = (EnumSingleton) declaredConstructor.newInstance("dencycheng", 6666);

    }
}