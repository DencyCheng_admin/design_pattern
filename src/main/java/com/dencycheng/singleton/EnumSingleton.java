package com.dencycheng.singleton;

/**
 * @author dencycheng
 * @date 2019/12/19 20:18
 */
public enum EnumSingleton {
    INSTANCE{
        @Override
        protected void printTest(){
            System.out.println("dencycheng print test");
        }
    };

    protected abstract void printTest();

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static EnumSingleton getInstance(){
        return INSTANCE;
    }
}
