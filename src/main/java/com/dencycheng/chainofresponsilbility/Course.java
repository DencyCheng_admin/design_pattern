package com.dencycheng.chainofresponsilbility;

/**
 * 课程类
 *
 * @Author dencycheng
 * @Date 2020/2/16 17:07
 */
public class Course {

    private String name;
    private String article;
    private String video;
    private String img;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
