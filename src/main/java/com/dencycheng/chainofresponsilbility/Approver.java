package com.dencycheng.chainofresponsilbility;

/**
 * 批准者
 *
 * @Author dencycheng
 * @Date 2020/2/16 17:09
 */
public abstract class Approver {
    protected Approver apprOver;

    /**
     * 设置下一个步骤
     * @param apprOver
     */
    public void setNextApprOver(Approver apprOver) {
        this.apprOver = apprOver;
    }

    /**
     * 发布课程
     * @param course
     */
    public abstract void deploy(Course course);
}
