package com.dencycheng.chainofresponsilbility;

/**
 * @Author dencycheng
 * @Date 2020/2/16 17:13
 */
public class ArticleApprover extends Approver {
    @Override
    public void deploy(Course course) {
        if (course.getArticle() != null) {
            System.out.println(course.getName() + "含有手记，批准");
            if(super.apprOver !=null){
                apprOver.deploy(course);
            }
        }else {
            System.out.println(course.getName() + "未含有手记，不批准");
            return;
        }
    }
}
