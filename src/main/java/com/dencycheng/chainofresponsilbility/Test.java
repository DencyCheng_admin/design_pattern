package com.dencycheng.chainofresponsilbility;

/**
 * @Author dencycheng
 * @Date 2020/2/16 17:19
 */
public class Test {
    public static void main(String[] args) {
        Approver articleApprover = new ArticleApprover();
        Approver videoApprover = new VideoApprover();
        Approver imgApprover = new ImgApprover();

        Course course = new Course();
        course.setName("java 设计模式");
        course.setArticle("java 设计模式的手记");
//        course.setVideo("java 设计模式的视频");

        articleApprover.setNextApprOver(videoApprover);
        videoApprover.setNextApprOver(imgApprover);
        articleApprover.deploy(course);
    }
}
