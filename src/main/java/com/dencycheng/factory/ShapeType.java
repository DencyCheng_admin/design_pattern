package com.dencycheng.factory;

/**
 * @author dencycheng
 * @date 2019/12/5 17:45
 */
public enum ShapeType {

    /**
     * 长方形
     */
    RECTANGLE("RECTANGLE"),
    /**
     *正方形
     */
    SQUARE("SQUARE"),
    /**
     * 圆
     */
    CIRCLE("CIRCLE");

    /**
     * 类型
     */
    private String type;

    ShapeType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
