package com.dencycheng.factory;

/**
 * @author dencycheng
 * @date 2019/12/3 14:19
 */
public class ShapeFactory {

    /**
     *   使用 getShape 方法获取形状类型的对象
     */
    public Shape getShape(String shapeType){
        if(shapeType == null){
            return null;
        }
        if(shapeType.equalsIgnoreCase(ShapeType.CIRCLE.getType())){
            return new Circle();
        } else if(shapeType.equalsIgnoreCase(ShapeType.RECTANGLE.getType())){
            return new Rectangle();
        } else if(shapeType.equalsIgnoreCase(ShapeType.SQUARE.getType())){
            return new Square();
        }
        return null;
    }
}
