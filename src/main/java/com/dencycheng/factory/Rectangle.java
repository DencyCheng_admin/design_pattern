package com.dencycheng.factory;

/**
 * @author dencycheng
 * @date 2019/12/5 17:42
 */
public class Rectangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside Rectangle::draw() method.");
    }
}
