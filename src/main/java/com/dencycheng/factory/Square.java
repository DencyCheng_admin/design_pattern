package com.dencycheng.factory;

/**
 * @author dencycheng
 * @date 2019/12/5 17:42
 */
public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
