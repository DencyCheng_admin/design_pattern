package com.dencycheng.prototype;

import java.text.MessageFormat;

/**
 * @Author dencycheng
 * @Date 2020/2/17 19:38
 */
public class MailUtil {
    public static void sendMail(Mail mail) {
        String content = "向{0}发送邮件。邮件地址：{1}。邮件内容：{2}";
        System.out.println(MessageFormat.format(content, mail.getName(), mail.getAddress(), mail.getContent()));
    }

    public static void saveOriginMailRecord(Mail mail) {
        System.out.println("存储originMail记录：" + mail.getContent());
    }
}
