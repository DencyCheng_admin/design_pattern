package com.dencycheng.prototype.clone;

import java.util.ArrayList;

/**
 * @Author dencycheng
 * @Date 2020/2/17 20:00
 */
public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {
//        Date birthday = new Date(0L);
//        Pig pig1 = new Pig("佩奇",birthday);
//        Pig pig2 = (Pig) pig1.clone();
//        System.out.println(pig1);
//        System.out.println(pig2);
//
//        pig1.getBirthday().setTime(66666666666L);
//
//        System.out.println(pig1);
//        System.out.println(pig2);


        ArrayList list1 = new ArrayList<>();
        ArrayList list2 = (ArrayList) list1.clone();

        System.out.println(list1);
        System.out.println(list2);


        System.out.println(list1);
        System.out.println(list2);
    }
}
