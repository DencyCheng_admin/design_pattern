package com.dencycheng.prototype;

/**
 * @Author dencycheng
 * @Date 2020/2/17 19:40
 */
public class Test {
    public static void main(String[] args) throws CloneNotSupportedException {
        Mail mail = new Mail();
        mail.setContent("初始化模板");
        for (int i = 0; i < 10; i++) {
            Mail clone = (Mail) mail.clone();
            clone.setName("同学"+i);
            clone.setAddress("同学"+i+"@qq.com");
            clone.setContent("恭喜中奖");
            MailUtil.sendMail(clone);
        }
        MailUtil.saveOriginMailRecord(mail);
    }
}
